package com.maxbill.base.service;

import com.maxbill.base.bean.Connect;
import com.maxbill.base.mapper.DataMapper;
import com.maxbill.util.KeyUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * 中心窗口
 *
 * @author MaxBill
 * @date 2019/07/06
 */
@Service
public class ConnectServiceImpl implements ConnectService {

    @Autowired
    private DataMapper dataMapper;

    /**
     * 查询连接列表
     */
    @Override
    public void initConnectData() {
        Connect connect = new Connect();
        connect.setId(KeyUtil.getKey());
        connect.setText("测试本地环境");
        dataMapper.insertConnect(connect);
    }


    /**
     * 查询连接列表
     */
    @Override
    public List<Connect> selectConnectList() {
        return dataMapper.selectConnectList();
    }

}

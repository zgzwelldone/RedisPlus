package com.maxbill.fxui.left;

import com.maxbill.base.bean.Connect;
import com.maxbill.base.bean.TreeNode;
import com.maxbill.base.service.ConnectService;
import com.maxbill.fxui.body.DataWindow;
import com.maxbill.fxui.foot.FootWindow;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeView;
import javafx.scene.input.MouseButton;

import java.util.List;

import static com.maxbill.fxui.util.CommonConstant.*;
import static com.maxbill.fxui.util.CommonFxuiUtil.getNodeIcon;
import static com.maxbill.fxui.util.DesktopObjects.context;
import static com.maxbill.util.RedisUtil.getDbList;
import static com.maxbill.util.RedisUtil.openJedis;
import static com.maxbill.util.StringUtil.isNull;

/**
 * 左部窗口
 *
 * @author MaxBill
 * @date 2019/07/06
 */
public class LeftWindow {

    private static TreeView<TreeNode> leftWindow;

    public static TreeView getLeftWindow() {
        leftWindow = new TreeView<>();
        leftWindow.setShowRoot(false);
        leftWindow.setPrefWidth(250);
        leftWindow.setId("left-pane");
        leftWindow.getStylesheets().add(STYLE_LEFT);
        leftWindow.getStylesheets().add(STYLE_SBAR);
        leftWindow.setFixedCellSize(30);

        // 定义服务连接
        TreeItem<TreeNode> serversTree = new TreeItem<>();
        TreeItem<TreeNode> singlesTree = new TreeItem<>();
        singlesTree.setExpanded(true);
        singlesTree.setValue(new TreeNode(MESSAGE_LEFT_SINGLES));
        singlesTree.setGraphic(getNodeIcon(LEFT_TREE_01));
        TreeItem<TreeNode> clusterTree = new TreeItem<>();
        clusterTree.setExpanded(true);
        clusterTree.setValue(new TreeNode(MESSAGE_LEFT_CLUSTER));
        clusterTree.setGraphic(getNodeIcon(LEFT_TREE_02));

        // 加载连接数据
        List<Connect> dataList = context.getBean(ConnectService.class).selectConnectList();
        if (null != dataList && !dataList.isEmpty()) {
            for (Connect connect : dataList) {
                TreeItem<TreeNode> treeItem = new TreeItem<>(new TreeNode(connect));
                treeItem.setGraphic(getNodeIcon(LEFT_TREE_03));
                if ("0".equals(connect.getIsha())) {
                    singlesTree.getChildren().add(treeItem);
                } else {
                    clusterTree.getChildren().add(treeItem);
                }
            }
        }
        serversTree.getChildren().add(singlesTree);
        serversTree.getChildren().add(clusterTree);
        leftWindow.setRoot(serversTree);

        // 监听事件
        leftWindow.setOnMouseClicked(event -> {
            // 单机事件
            if (event.getClickCount() == 1) {
                dealSingleClick();
            }
            if (event.getClickCount() == 2) {
                dealDoubleClick();
            }
            //右击事件
            if (event.getButton() == MouseButton.SECONDARY) {
                System.out.println("123");
            }
        });

        return leftWindow;
    }

    /**
     * 处理单机事件
     */
    private static void dealSingleClick() {
        TreeItem<TreeNode> currItem = leftWindow.getSelectionModel().getSelectedItem();
        TreeNode currNode = currItem.getValue();
        // 是否是双击的服务项
        if (!currNode.getType().equals(DATA_TREE_TYPE_D)) {
            return;
        }
        // 数据库索引值
        int index = (Integer) currNode.getData();
        DataWindow.initDataTab(currNode.getName(), index);
    }

    /**
     * 处理双击事件
     */
    private static void dealDoubleClick() {
        TreeItem<TreeNode> currItem = leftWindow.getSelectionModel().getSelectedItem();
        TreeNode currNode = currItem.getValue();
        // 是否是双击的服务项
        if (!currNode.getType().equals(DATA_TREE_TYPE_S)) {
            return;
        }
        // 已经是打开状态
        if (!currItem.isLeaf()) {
            return;
        }
        Connect connect = (Connect) currNode.getData();
        // 打开连接
        String openMsg = openJedis(connect);
        if (!isNull(openMsg)) {
            FootWindow.changeFootState(false, openMsg);
        } else {
            FootWindow.changeFootState(true, "已连接到:".concat(connect.getText()));
        }
        // 获取数据库数据
        List<TreeNode> nodeList = getDbList();
        if (null != nodeList && !nodeList.isEmpty()) {
            for (TreeNode treeNode : nodeList) {
                TreeItem<TreeNode> treeItem = new TreeItem<>(treeNode);
                treeItem.setGraphic(getNodeIcon(LEFT_TREE_04));
                currItem.getChildren().add(treeItem);
            }
        }
        // 展开数据
        currItem.setExpanded(true);
    }

}
